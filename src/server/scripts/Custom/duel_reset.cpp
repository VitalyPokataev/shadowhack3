/*
 * Copyright (C) 2013-2014 ShadowHack3 <https://bitbucket.org/Vitasic/shadowhack3>
 */

#include "ScriptPCH.h"
#include "ScriptMgr.h"
#include "Config.h"

class duel_reset : public PlayerScript
{
public:
    duel_reset() : PlayerScript("duel_reset") {}

	void OnDuelEnd(Player *winner, Player *looser, DuelCompleteType type)
	{
		if (type == DUEL_WON)
		{
			winner->RemoveArenaSpellCooldowns();
			looser->RemoveArenaSpellCooldowns();
			winner->SetHealth(winner->GetMaxHealth());
			looser->SetHealth(looser->GetMaxHealth());

			if (winner->getPowerType() == POWER_MANA)
				winner->SetPower(POWER_MANA, winner->GetMaxPower(POWER_MANA));
			if (looser->getPowerType() == POWER_MANA)
				looser->SetPower(POWER_MANA, looser->GetMaxPower(POWER_MANA));
		}
		winner->HandleEmoteCommand(EMOTE_ONESHOT_CHEER);
	}

	void OnDuelStart(Player *player1, Player *player2)
	{
		player1->RemoveArenaSpellCooldowns();
		player2->RemoveArenaSpellCooldowns();
		player1->SetHealth(player1->GetMaxHealth());
		player2->SetHealth(player2->GetMaxHealth());

		if (player1->getPowerType() == POWER_MANA)
			player1->SetPower(POWER_MANA, player1->GetMaxPower(POWER_MANA));
		if (player2->getPowerType() == POWER_MANA)
			player2->SetPower(POWER_MANA, player2->GetMaxPower(POWER_MANA));

		if (player1->getPowerType() == POWER_RAGE)
			player1->SetPower(POWER_RAGE, 0);
		if (player2->getPowerType() == POWER_RAGE)
			player2->SetPower(POWER_RAGE, 0);
		if (player1->getPowerType() == POWER_RUNIC_POWER)
			player1->SetPower(POWER_RUNIC_POWER, 0);
		if (player2->getPowerType() == POWER_RUNIC_POWER)
			player2->SetPower(POWER_RUNIC_POWER, 0);
	}
};

void AddSC_DuelReset()
{
    new duel_reset();
}
